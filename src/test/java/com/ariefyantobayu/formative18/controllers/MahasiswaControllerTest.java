package com.ariefyantobayu.formative18.controllers;

import com.ariefyantobayu.formative18.models.Mahasiswa;
import com.ariefyantobayu.formative18.repositories.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MahasiswaControllerTest {

    @Mock
    MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    MahasiswaController mahasiswaController;

    @Test
    @DisplayName("Test Get All Mahasiswa Data")
    void getAllMahasiswaTest() {
        when(mahasiswaRepository.getAll()).thenReturn(List.of(
                new Mahasiswa(1, "NIK-1231231231230", "Bayu Seno A", 22),
                new Mahasiswa(2, "NIK-1231231231231", "D. Aurelia Y", 23),
                new Mahasiswa(3, "NIK-1231231231232", "Kenny P", 24)
        ));

        assertEquals(3, mahasiswaController.getAll().size());
    }

    @Test
    @DisplayName("Test Get One Mahasiswa By Id Success")
    void getMahasiswaByIdSuccessTest() {
        Mahasiswa mahasiswa = new Mahasiswa(2, "NIK-1231231231231", "D. Aurelia Y", 23);
        when(mahasiswaRepository.getById(anyLong())).thenReturn(mahasiswa);
        Mahasiswa mahasiswaTest = mahasiswaController.getById(3);
        assertEquals(mahasiswa.getId(), mahasiswaTest.getId());
        assertEquals(mahasiswa.getIdentityNumber(), mahasiswaTest.getIdentityNumber());
        assertEquals(mahasiswa.getName(),mahasiswaTest.getName());
        assertEquals(mahasiswa.getAge(), mahasiswaTest.getAge());
    }

    @Test
    @DisplayName("Test Get One Mahasiswa By Id Failed")
    void getMahasiswaByIdFailedTest() {
        when(mahasiswaRepository.getById(anyLong())).thenReturn(null);
        assertNull(mahasiswaController.getById(2));
    }

    @Test
    @DisplayName("Test Success Add One Mahasiswa")
    void addMahasiswaSuccess() {
        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(0L);
        assertEquals(0L, mahasiswaController.add("NIK-1231231231230", "Bayu", "22"));
    }

    @Test
    @DisplayName("Failed Add Mahasiswa Invalid Params")
    void addMahasiswaFailed() {
        when(mahasiswaRepository.save(any(Mahasiswa.class))).thenReturn(1L);
        assertNotEquals(1L, mahasiswaController.add("NIK-1231231231230", "Bayu2", "22"));
    }


    @Test
    @DisplayName("Delete Mahasiswa By Id")
    void deleteMahasiswaSuccess() {
    	Mahasiswa mahasiswa = new Mahasiswa(2, "NIK-1231231231231", "D. Aurelia Y", 23);
    	when(mahasiswaRepository.deleteById(anyLong())).thenReturn(mahasiswa);
    	assertNotNull(mahasiswaController.remove(2L));
    }
}