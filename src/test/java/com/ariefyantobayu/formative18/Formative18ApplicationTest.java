package com.ariefyantobayu.formative18;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Formative18ApplicationTest {

    @Test
    @DisplayName("Application Running Succesfuly Test")
    void applicationRunningSuccessfullyTest() {
        assertDoesNotThrow(() -> {
            Formative18Application.main(new String[]{});
        });
    }
}