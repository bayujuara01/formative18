package com.ariefyantobayu.formative18.models;

import java.util.Random;

public class Mahasiswa {
    private final Random random = new Random(System.currentTimeMillis());
    private long id;
    private String identityNumber;
    private String name;
    private int age;

    public Mahasiswa(String identityNumber, String name, int age) {
        this.id = random.nextInt(1000);
        this.identityNumber = identityNumber;
        this.name = name;
        this.age = age;
    }

    public Mahasiswa(long id, String identityNumber, String name, int age) {
        this.id = id;
        this.identityNumber = identityNumber;
        this.name = name;
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
