package com.ariefyantobayu.formative18.controllers;

import com.ariefyantobayu.formative18.models.Mahasiswa;
import com.ariefyantobayu.formative18.repositories.MahasiswaRepository;

import java.util.List;


public class MahasiswaController {
    private final MahasiswaRepository mahasiswaRepository;

    public MahasiswaController(MahasiswaRepository mahasiswaRepository) {
        this.mahasiswaRepository = mahasiswaRepository;
    }

    public Mahasiswa getById(long id) {
        return mahasiswaRepository.getById(id);

    }

    public List<Mahasiswa> getAll() {
        return mahasiswaRepository.getAll();
    }

    public long add(String identityNumber, String name, String age) {
        Mahasiswa mahasiswa;
        long insertId = -1;

        if (isValidIdentityNumber(identityNumber) && isValidName(name) && isValidAge(age)) {
            int ageInt = Integer.parseInt(age);
            mahasiswa = new Mahasiswa(identityNumber, name, ageInt);
            insertId = mahasiswaRepository.save(mahasiswa);
        }

        return insertId;
    }

    public Mahasiswa remove(long id) {
        return mahasiswaRepository.deleteById(id);
    }

    private boolean isValidIdentityNumber(String identityNumber) {
        return identityNumber.matches("NIK-[0-9]{13}");
    }

    private boolean isValidName(String name) {
        return name.matches("^[a-zA-Z ]+$");
    }

    private boolean isValidAge(String age) {
        return age.matches("(1(50|[0-4][0-9]|[0-9])?)|([2-9][0-9])");
    }
}