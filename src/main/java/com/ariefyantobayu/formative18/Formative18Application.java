package com.ariefyantobayu.formative18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative18Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative18Application.class, args);
	}

}
