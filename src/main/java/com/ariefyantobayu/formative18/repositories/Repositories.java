package com.ariefyantobayu.formative18.repositories;

import java.util.List;

public interface Repositories<T> {
    T getById(long id);
    List<T> getAll();
    long save(T t);
    T deleteById(long id);
}
