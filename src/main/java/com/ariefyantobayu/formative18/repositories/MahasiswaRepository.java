package com.ariefyantobayu.formative18.repositories;

import com.ariefyantobayu.formative18.models.Mahasiswa;

public interface MahasiswaRepository extends Repositories<Mahasiswa> {
    Mahasiswa getByIdentityNumber(String identityNumber);
}
